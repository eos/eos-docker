#!/usr/bin/env bash

usage()
{
  echo "Usage:"
  echo "$(basename $0) <fsid> [-u <uuid>] [-d <mountpoint>] [-s <space>] [-c <configstatus>] [-g geotag]"
  echo
  echo "-h	show usage and exit"
  echo
}

[[ $1 = -h ]] && usage && exit 0

id=$1
shift

if [[ -z $id ]]; then
  echo -e "Filesystem ID (fsid) must be specified.\n"
  usage
  exit 1
fi

UUID=fst${id}
DATADIR=/home/data/eos${id}
SPACE=default
CONFIG=rw
GEOTAG=""
FSTHOSTNAME=$(hostname -f)

while getopts 'u:d:s:c:g:' flag; do
  case "${flag}" in
    u) UUID="${OPTARG}" ;;
    d) DATADIR="${OPTARG}" ;;
    s) SPACE="${OPTARG}" ;;
    c) CONFIG="${OPTARG}" ;;
    g) GEOTAG="${OPTARG}" ;;
    *) usage
           exit 1;;
  esac
done

# If specified, set new geotag instead of default one for FST server
[[ -n $GEOTAG ]] && sed -i "s/EOS_GEOTAG=.*/EOS_GEOTAG=$GEOTAG/" /etc/sysconfig/eos

source /etc/sysconfig/eos
export EOS_MGM_URL=root://eos-mgm1.eoscluster.cern.ch//
# Enable stacktrace collection for FSTS
export EOS_FST_ENABLE_STACKTRACE=1
# Enable temporarily full XRootD logs
#export XRD_LOGLEVEL=Dump

if ! grep "platform:el9" /etc/os-release; then
  yum install -y devtoolset-9-gdb
  source /opt/rh/devtoolset-9/enable
fi

# Enable coredumps
sysctl -w kernel.core_pattern=/tmp/core-%e.%p.%h.%t
sysctl -w fs.suid_dumpable=2

if [ -e /opt/eos/xrootd/bin/xrootd ]; then
   XROOTDEXE="/opt/eos/xrootd/bin/xrootd"
else
   XROOTDEXE="/usr/bin/xrootd"
fi


echo "Starting fst${id} ..."
${XROOTDEXE} -n fst${id} -c /etc/xrd.cf.fst -l /var/log/eos/xrdlog.fst -b -Rdaemon
echo "Start xrootd process return code: $?"
echo "Configuration start for fst${id} ..."
mkdir -p $DATADIR
echo "$UUID" > $DATADIR/.eosfsuuid
echo "${id}" > $DATADIR/.eosfsid
chown -R daemon:daemon $DATADIR
# Give some time to the FST to start and then register with the MGM
sleep 1
eos -r 0 0 -b fs add -m ${id} $UUID $FSTHOSTNAME:1095 $DATADIR spare $CONFIG
eos -r 0 0 -b fs mv --force ${id} $SPACE
eos -r 0 0 -b node set $FSTHOSTNAME:1095 on
echo "Configuration done for fst${id}"
