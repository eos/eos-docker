#!/usr/bin/env bash

source /etc/sysconfig/eos

# Preload jemalloc
jemalloc=$(find /usr/lib64/ -name "libjemalloc*" | sort | tail -1)

if [[ ! -z "${jemalloc}" ]]; then
  export LD_PRELOAD=${jemalloc}
fi

chown daemon:daemon /etc/eos.krb5.keytab

if [ -e /opt/eos/xrootd/bin/xrootd ]; then
   XROOTDEXE="/opt/eos/xrootd/bin/xrootd"
else
   XROOTDEXE="/usr/bin/xrootd"
fi

${XROOTDEXE} -n mgm -c /etc/xrd.cf.mgm -m -l /var/log/eos/xrdlog.mgm -b -Rdaemon

# Enable sss authentication for the FSTs to connect to the MGM
eos -b vid enable sss
eos -b vid enable krb5
eos -b vid enable gsi
eos -b vid enable https
# Allow commands executed from the FSTs to change to root role
eos -b vid set membership 2 +sudo

# Make instace root directory world accessible
eos -b chmod 2777 /eos/dockertest/

# let the force be with admin1 (typically krb-authenticated on clients)
adduser admin1
eos -b vid set membership admin1 +sudo
eos -b space define default 8 1
