#
# Simple EOS Docker file
#

FROM centos:7
LABEL maintainer="Elvin Sindrilaru, esindril@cern.ch, CERN 2017"

ARG EOS_CODENAME

# Expect to use eos-xrootd in running containers
ENV PATH "/opt/eos/xrootd/bin:${PATH}"
ENV LD_LIBRARY_PATH "/opt/eos/xrootd/lib64:${LD_LIBRARY_PATH}"

# Add extra repositories
COPY eos-docker/el-7/*.repo /etc/yum.repos.d/

# Add helper scripts
COPY eos-docker/image_scripts/*.sh /

# Add configuration files for EOS instance
COPY eos-docker/eos.sysconfig /etc/sysconfig/eos
COPY eos-docker/xrd.cf.* eos-docker/krb5.conf /etc/
COPY eos-docker/fuse.eosdockertest.conf /etc/eos/fuse.eosdockertest.conf
COPY eos-docker/fuse.conf /etc/eos/fuse.mount-1.conf
COPY eos-docker/fuse.conf /etc/eos/fuse.mount-2.conf

# Add configuration files for forwarding proxy server
COPY eos-docker/xrootd.conf /etc/tmpfiles.d/
COPY eos-docker/xrootd-fwd-proxy.cfg /etc/xrootd/

# Set correct path for the dependency repository based on the EOS version
# which is being built
RUN sed -i "s/__EOS_CODENAME__/${EOS_CODENAME}/g" /etc/yum.repos.d/eos.repo

RUN mkdir /var/tmp/eosxd-cache/ /var/tmp/eosxd-journal/
RUN adduser eos-user && adduser eosnobody

# Fix CentOS7 repository after the EOL
RUN sed -i -e '/mirrorlist/d' -e 's/# \?baseurl=/baseurl=/g' -e 's/mirror.centos.org/vault.centos.org/g' -e 's/$releasever/7.9.2009/g' /etc/yum.repos.d/*.repo

# Docker will aggressively cache the following command, but this is fine, since
# these packages are not updated often.
RUN yum -y --nogpg install epel-release cerntos-release-scl-rh && \
    sed -i -e '/mirrorlist/d' -e 's/# \?baseurl=/baseurl=/g' -e 's/mirror.centos.org/vault.centos.org/g' -e 's/$releasever/7.9.2009/g' /etc/yum.repos.d/*.repo
RUN yum -y --nogpg install at bzip2 cmake3 compat-libf2c-34 \
    createrepo gcc-c++ gdb git heimdal-server \
    heimdal-workstation initscripts krb5-server krb5-workstation less \
    libgfortran  parallel perl-Test-Harness python2 python2-pip \
    python3 python3-pip redhat-lsb-core redis rpm-build sudo xauth \
    yum-plugin-priorities davix openssl rsync voms-clients-cpp sqlite \
# Used by eosclient-tests
    libtool autoconf automake \
    && yum clean all

# Install new EOS from created repo - the ADD command will reset the docker cache,
# and any commands after that point will be uncached.
ENV EOSREPODIR="/repo/eos"
ADD cc7_artifacts/RPMS/ ${EOSREPODIR}

# Special packages, must be installed un-cached.
RUN createrepo ${EOSREPODIR} \
    && echo -e "[eos-artifacts]\nname=EOS artifacts\nbaseurl=file://${EOSREPODIR}\ngpgcheck=0\nenabled=1\npriority=1" >> /etc/yum.repos.d/eos.repo \
    && yum -y --nogpg install eos-server eos-client eos-archive eos-fusex \
                              eos-ns-inspect eos-test eos-testkeytab eos-grpc-gateway \
    && yum clean all

RUN if [ "${EOS_CODENAME}" != "diopside" ]; \
    then yum -y --nogpg install eos-fuse quarkdb; \
    else yum -y --nogpg install eos-quarkdb; fi

# Create macaroon secret and set sss keytab permissions to400
RUN openssl rand -base64 -out /etc/eos.macaroon.secret 64 \
    && chown 2:2 /etc/eos.macaroon.secret \
    && chmod 400 /etc/eos.keytab /etc/eos.client.keytab /etc/eos.macaroon.secret

# Generate only a common root CA certificate
RUN /mkcert-ssl.sh -s
ENTRYPOINT ["/bin/bash"]
