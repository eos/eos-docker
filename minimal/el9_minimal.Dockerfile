#
# Simple EOS Docker file
#
# Version 0.1

FROM gitlab-registry.cern.ch/linuxsupport/alma9-base
LABEL maintainer="Elvin Sindrilaru, esindril@cern.ch, CERN 2024"

# One of {citrine, diopside}
ARG EOS_CODENAME=diopside
# One of {commit, tag-testing, tag}
ARG REPOBRANCH=tag

# Expect to use eos-xrootd in running containers
ENV PATH /opt/eos/xrootd/bin:$PATH
ENV LD_LIBRARY_PATH /opt/eos/xrootd/lib64:$LD_LIBRARY_PATH

COPY eos-docker/minimal/el-9/eos.repo /etc/yum.repos.d/eos.repo
RUN sed -i "s|__EOS_CODENAME__|${EOS_CODENAME}|g" /etc/yum.repos.d/eos.repo \
    && dnf config-manager --enable "eos-${REPOBRANCH}"

RUN dnf -y --nogpg install epel-release && \
    dnf -y --nogpg install \
    eos-archive eos-client eos-fusex eos-ns-inspect \
    eos-server eos-test eos-testkeytab davix eos-quarkdb

# sss keytabs needs to be 400
RUN chmod 400 /etc/eos.keytab /etc/eos.client.keytab

# Install some much needed utility: please bloat with care!
RUN dnf -y --nogpg install nano redis && dnf clean all && rm -rf /var/cache/yum
ENTRYPOINT ["/bin/bash"]
